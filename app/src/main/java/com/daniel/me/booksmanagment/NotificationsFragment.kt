package com.daniel.me.booksmanagment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.Button
import kotlinx.android.synthetic.main.fragment_notifications.*
import java.util.logging.Level
import java.util.logging.Logger

class NotificationsFragment : Fragment() {

    var clickedTimes = 0

    private val buttonOnClickedListener = OnClickListener {
        clickedTimes++
        labels.text = resources.getString(R.string.name, "Something");
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_notifications, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        if(incrementButton ==  null) {
            Logger.getAnonymousLogger().log(Level.SEVERE, "Hey, it's null")
        } else {
            Logger.getAnonymousLogger().log(Level.INFO, "Hey, initiating")
            incrementButton.setOnClickListener(this.buttonOnClickedListener)
        }
    }

    override fun onResume() {
        super.onResume()
        Logger.getAnonymousLogger().log(Level.INFO, "Hey, working")
        labels.setText("clickedTimes" + clickedTimes)

    }


}
