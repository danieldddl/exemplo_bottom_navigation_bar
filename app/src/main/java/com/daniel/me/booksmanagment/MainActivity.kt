package com.daniel.me.booksmanagment

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.design.internal.BottomNavigationItemView
import android.support.design.internal.BottomNavigationMenuView
import android.support.design.widget.BottomNavigationView
import android.support.design.widget.BottomNavigationView.OnNavigationItemSelectedListener
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.reflect.Field
import java.util.logging.Level
import java.util.logging.Logger

class MainActivity : AppCompatActivity() {

    var homeFragment : HomeFragment? = null
    var dashboardFragment: DashboardFragment? = null
    var notificationsFragment: NotificationsFragment? = null

    private val mOnNavigationItemSelectedListener = OnNavigationItemSelectedListener { item ->

        when (item.itemId) {
            R.id.navigation_home -> {
                if(this.homeFragment == null) {
                    this.homeFragment = HomeFragment()
                }

                loadFragment(this.homeFragment)
            }
            R.id.navigation_dashboard -> {
                if(this.dashboardFragment == null) {
                    this.dashboardFragment = DashboardFragment()
                }

                loadFragment(this.dashboardFragment)
            }
            R.id.navigation_notifications -> {
                if(this.notificationsFragment == null) {
                    this.notificationsFragment= NotificationsFragment()
                }

                loadFragment(this.notificationsFragment)
            }
        }

        true
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        loadFragment(HomeFragment())

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        MainActivity.removeShiftMode(navigation)
    }

    private fun loadFragment(fragment: Fragment?): Boolean {

        //switching fragment
        if (fragment != null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit()
            return true
        }

        return false
    }

    companion object BottomNavigationViewHelper {

        //TODO understand
        @SuppressLint("RestrictedApi")
        fun removeShiftMode (view : BottomNavigationView) {

            val FIRST_ELEMENT = 0
            var SHIFTING_MODE_FIELD = "mShiftingMode"

            var menuView : BottomNavigationMenuView = view.getChildAt(FIRST_ELEMENT) as BottomNavigationMenuView

            var shiftingMode : Field = menuView.javaClass.getDeclaredField(SHIFTING_MODE_FIELD)
            shiftingMode.isAccessible = true
            shiftingMode.setBoolean(menuView, false)

            for (i in 0 until menuView.childCount) {
                var item : BottomNavigationItemView = menuView.getChildAt(i) as BottomNavigationItemView
                item.setShiftingMode(false)
                item.setChecked(item.itemData.isChecked)

            }
        }

    }

}
